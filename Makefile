NAME = libflattable.a

NICK = FHT
PROJECT_COLOR = "\033[38;5;033m"
PROJECT_COLOR_ALT = "\033[38;5;027m"

CC = clang

SRCS =	\
		$(PATH_SRC)/find.c													\
		$(PATH_SRC)/set.c													\
		$(PATH_SRC)/hash.c													\
		$(PATH_SRC)/init.c													\
		$(PATH_SRC)/insert.c												\
		$(PATH_SRC)/destroy.c												\
		$(PATH_SRC)/remove.c												\
		$(PATH_SRC)/expand.c												\
		$(PATH_SRC)/iterate.c												\
		\
		$(PATH_SRC)/utils.c													\

INC = $(PATH_INC)

# Paths                                      #

PATH_SRC := src
PATH_INC := include
PATH_OBJ := .obj
PATH_DEP := .dep

# Compilation                                  #

CLIBS = $(foreach dep, $(LIBS), $(dep)/$(notdir $(dep)).a) \

CFLAGS = $(foreach inc, $(INC), -I$(inc)) \
		 $(foreach dep, $(LIBS), -I$(dep)/$(PATH_INC)) \
		 $(foreach dep, $(LDLIBS), -I$(dep)/$(PATH_INC)) \

# Various                                    #

SHELL = bash

# Variables Customizers                            #

OBJS := $(patsubst %.c, $(PATH_OBJ)/%.o, $(SRCS))
DEPS := $(patsubst %.c, $(PATH_DEP)/%.d, $(SRCS))

# Includes

include makefiles/lib.mk
include makefiles/test.mk
include makefiles/strings.mk
include makefiles/depend.mk
