#include "../include/flattable.h"

bool		__ftable_expand(
		t_ftable_generic *table,
		size_t new_capacity,
		size_t key_size,
		size_t value_size
)
{

	// TODO auto-generated method stub.
	// This will seem stupid and it is.
	// To expand a table we essentily create a new one and repopulate it.

	printf("Expand table\n");
	t_ftable_generic new = (t_ftable_generic){
		.data = malloc((FT_CONTROL_SIZE
					+ FT_GROUP_SIZE * (key_size + value_size))
				* (new_capacity)),
		.capacity = new_capacity,
	};
	// Failed to allocate
	if (!new.data)
		return false;
	// Init
	if (!__ftable_init(&new, key_size, value_size))
		return false;

	t_ftable_iterator it = ftable_iterator;
	uint8_t key[key_size], value[value_size];
	while ((__ftable_iterate(table, &it, (void**)key, (void**)value, key_size, value_size)))
	{
		__ftable_insert(&new, key, value, key_size, value_size, table->hash(key, new.capacity));
	}
	free(table->data);
	table->capacity = new.capacity;
	table->data = new.data;
	return true;
}
