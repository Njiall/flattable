#include "../include/flattable.h"
#include <string.h>

#if __SSE2__
#  include <emmintrin.h>
#  include <immintrin.h>
typedef __m128i			t_ftable_128i;

static inline uint16_t	__get_ctrl_mask(
		uint8_t mask,
		t_ftable_128i ctrl
)
{
	return (_mm_movemask_epi8(
				_mm_cmpeq_epi8(
					_mm_set1_epi8(mask), ctrl)
				)
			);
}
#else
#pragma message "Missing SSE2 instruction set, using slower fallback."
typedef uint8_t			t_ftable_128i[FT_CONTROL_SIZE];

static inline uint16_t	__get_ctrl_mask(
		uint8_t mask,
		t_ftable_128i ctrl
)
{
	int	i;
	int	out;

	out = 0;
	i = FT_CONTROL_SIZE;
	while (i--)
		if (control[i] == mask)
			out |= 1 << i;
	return ((uint16_t)out);
}
#endif

bool					__ftable_iterate(
		t_ftable_generic *table,
		t_ftable_iterator *it,
		void **key,
		void **value,
		size_t key_size,
		size_t value_size
)
{
	// Bucket searching
	for (; it->index < table->capacity; ++it->index) {
		void *group = FT_GROUP_MEM(table->ctrl, it->index, key_size, value_size);

		// Since it copies the most significant bit to a 16 bit word, and it's
		// conviniently the way we tell it is empty,
		// tels if there is data
		// Bits are at 0 when there is
		uint16_t match = _mm_movemask_epi8(*(t_ftable_128i*)group);
		// Empty group doesn't need to be red.
		if (match == 0xffff) {
			// Don't forget to reset group index
			it->group = 0;
			continue;
		}
		// Group searching
		for (; it->group < FT_GROUP_SIZE; ++it->group)
		{
			if (!(match & (1 << it->group))) {
				memcpy(key, FT_KEY_MEM(group) + it->group * key_size, key_size);
				memcpy(value, FT_VALUE_MEM(group, key_size) + it->group * value_size, value_size);
				it->group++;
				return (true);
			}
		}
		// Reset group index
		it->group = 0;
	}
	return (false);
}
