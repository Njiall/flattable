#include "../include/flattable.h"
#include <stddef.h>

#if __SSE2__
#include <emmintrin.h>
#endif

bool __ftable_init(
		t_ftable_generic *table,
		size_t key_size,
		size_t value_size
)
{
	if (!table->data)
		return (false);
	for (size_t i = 0; i < table->capacity; ++i) {
		void *group = FT_GROUP_MEM(table->ctrl, i, key_size, value_size);
#if __SSE2__
		*(__m128i*)group = _mm_set1_epi8((char)FT_EMPTY);
#else
		for (size_t g = 0; g < FT_GROUP_SIZE; ++g)
			((uint8_t*)group)[g] = (uint8_t)FT_EMPTY;
#endif
	}
	return (true);
}
