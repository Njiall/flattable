#include "../include/flattable.h"

// Just to be clear this is the default comparison function to compare keys.
// to compare strings you should specify your own function
// or just use `strcmp`. It will work as well.
bool			__ftable_key_eq(void *a, void *b)
{
	return (a == b);
}
