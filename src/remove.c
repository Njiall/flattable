#include "../include/flattable.h"
#include <stdint.h>
#include <assert.h>

// These functions serve to analyze the control bits to know where the group
// we are searching is.
#if __SSE2__
#  include <emmintrin.h>
#  include <immintrin.h>
typedef __m128i			t_ftable_128i;

static inline uint16_t	__get_ctrl_mask(
		uint8_t mask,
		t_ftable_128i ctrl
)
{
	return (_mm_movemask_epi8(
				_mm_cmpeq_epi8(
					_mm_set1_epi8(mask), ctrl)
				)
			);
}
#else
#pragma message "Missing SSE2 instruction set, using slower fallback."
typedef uint8_t			t_ftable_128i[FT_CONTROL_SIZE];

static inline uint16_t	__get_ctrl_mask(
		uint8_t mask,
		t_ftable_128i ctrl
)
{
	int	i;
	int	out;

	out = 0;
	i = FT_CONTROL_SIZE;
	while (i--)
		if (control[i] == mask)
			out |= 1 << i;
	return ((uint16_t)out);
}
#endif

// Returns the value on remove.
// If there is no entry for the key or it's already deleted it will return
// false.
bool		__ftable_remove(
		t_ftable_generic *table,
		void *key,
		void **value,
		size_t key_size,
		size_t value_size,
		t_ft_hash hash,
		bool (*cmp)(void *, void *)
)
{
	size_t		index = hash.h1 & (table->capacity - 1);
	for (size_t i = 0; i < table->capacity; ++i) {
		// Start of the group we are searching in.
		// group {
		//     ctrl: 8 bits (FT_CONTROL_SIZE)
		//     keys: FT_GROUP_SIZE * key_size
		//     values: FT_GROUP_SIZE * value_size
		// }
		void *group = table->ctrl
			+ (FT_GROUP_SIZE
					+ key_size * FT_GROUP_SIZE
					+ value_size * FT_GROUP_SIZE)
			* index;

		// If there is a match in the group
		uint16_t match = __get_ctrl_mask(hash.h2, *(t_ftable_128i*)group);
		for (size_t i = 0; i < FT_CONTROL_SIZE; ++i) {
			// Find which is the slot of the match
			// And compare the key to be sure it's the right one
			if (match & (1 << i)
					&& __builtin_expect(
						cmp(key, FT_KEY_MEM(group) + i * key_size), true
					)
			) {
				memcpy(value, FT_VALUE_MEM(group, key_size) + i * value_size, value_size);
				((uint8_t*)group)[i] = FT_DELETED;
				table->pairs--;
				// Remove worked
				return (true);
			}
		}
		// There is no tombstone so there is no point in searching further.
		if (__builtin_expect(match != 0xffff, true))
			return (false);
		index = (index + (1 << i)) & (table->capacity - 1);
	}
	// We never get there :P
	// IF we get there we search the whole table without finding the item.
	assert(0);
}
