#include "../include/flattable.h"
#include <assert.h>

// These functions serve to analyze the control bits to know where the group
// we are searching is.
#if __SSE2__
# include <emmintrin.h>
# include <immintrin.h>
typedef __m128i			t_ftable_128i;

static inline uint16_t	__get_ctrl_mask(
		uint8_t mask,
		t_ftable_128i ctrl
)
{
	// This expand a 1 byte word to 16 bytes word
	// And compares the result to an other 16 bytes word
	// And flattens it to a 2 byte word:
	//
	// mask = 0xbe
	//    `-> 0xbebe bebe           ....           bebe
	// ctrl = 0xff00 ff00 febe ff00 ff00 ff00 ff00 fe00
	//      & 0b 0 0  0 0  0 1  0 0  0 0  0 0  0 0  0 0
	// result
	//    `-> 0b0000010000000000
	// But this only works when you have the `SSE2` instruction set.

	return (_mm_movemask_epi8(
				_mm_cmpeq_epi8(
					_mm_set1_epi8(mask), ctrl)
				)
			);
}

static inline uint16_t	__get_occupied(
		t_ftable_128i ctrl
)
{
	// Get the most significant bit of the 16 bytes and merge them
	// in a 16 bit word.
	return (
			_mm_movemask_epi8(ctrl)
	);
}
#else
#pragma message "Missing SSE2 instruction set, using slower fallback."
typedef uint8_t			t_ftable_128i[FT_CONTROL_SIZE];

static inline uint16_t	__get_ctrl_mask(
		uint8_t mask,
		t_ftable_128i ctrl
)
{
	int	i;
	int	out;

	out = 0;
	i = FT_CONTROL_SIZE;
	while (i--)
		if (control[i] == mask)
			out |= 1 << i;
	return ((uint16_t)out);
}

static inline uint16_t	__get_occupied(
		t_ftable_128i ctrl
)
{
	int	i;
	int	out;

	out = 0;
	i = FT_CONTROL_SIZE;
	while (i--)
		if (control[i] & 0b10000000)
			out |= 1 << i;
	return ((uint16_t)out);
}
#endif

bool			__ftable_set(
		t_ftable_generic *table,
		void *key,
		void *value,
		size_t key_size,
		size_t value_size,
		t_ft_hash hash,
		bool (*cmp)(void *, void *)
)
{
	// Expand if we need to put a new pair
	if (table->pairs >= FT_LOAD_FACTOR * table->capacity)
		if (!__ftable_expand(table, table->capacity << 1, key_size, value_size))
			return (false);

	// Find
	size_t index = hash.h1 & (table->capacity - 1);

	for (size_t i = 0; i < table->capacity; ++i) {
		void *group = FT_GROUP_MEM(table->ctrl, index, key_size, value_size);

		// If there is a match in the group
		uint16_t match = __get_ctrl_mask(hash.h2, *(t_ftable_128i*)group);
		for (size_t g = 0; g < FT_GROUP_SIZE; ++g) {
			if (match & (1 << g)
					&& __builtin_expect(
						cmp(key, FT_KEY_MEM(group) + g * key_size), true
					)
			) {
				memcpy(FT_VALUE_MEM(group, key_size) + g * value_size, value, value_size);
				table->pairs++;
				// find worked
				return (true);
			}
		}
		// If there is nothing in the group then we don't need to search further.
		if (__builtin_expect(match != 0xffff, true))
			break;
		index = (index + (1 << i)) & (table->capacity - 1);
	}
	// If not there insert at first spot

	// Reset index since we searched
	index = hash.h1 & (table->capacity - 1);
	for (size_t i = 0; i < table->capacity; ++i) {
		void *group = FT_GROUP_MEM(table->ctrl, index, key_size, value_size);

		uint16_t match;
		if ((match = __get_occupied(*(t_ftable_128i*)group))) {
			for (size_t g = 0; g < FT_CONTROL_SIZE; ++g) {
				// Find which is the slot of the match
				if (match & (1 << g)) {
					// Inserts meta data into control
					((uint8_t*)group)[g] = 0;
					((uint8_t*)group)[g] = hash.h2;
					// Inserts the key-value
					memcpy(FT_KEY_MEM(group) + g * key_size, key, key_size);
					memcpy(FT_VALUE_MEM(group, key_size) + g * value_size, value, value_size);
					// Update table pairs
					table->pairs++;
					// Insert worked
					return (true);
				}
			}
		}
		index = (index + (1 << i)) & (table->capacity - 1);
	}
	assert(!"We are NOT supposed to get there.");
}
