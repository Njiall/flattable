#include "../include/flattable.h"
#include <string.h>
#include <stdint.h>
#include <assert.h>

// These functions serve to analyze the control bits to know where the group
// we are searching is.
#if __SSE2__
#  include <emmintrin.h>
#  include <immintrin.h>
typedef __m128i			t_ftable_128i;

static inline uint16_t	__get_occupied(
		t_ftable_128i ctrl
)
{
	// Get the most significant bit of the 16 bytes and merge them
	// in a 16 bit word.
	return (
			_mm_movemask_epi8(ctrl)
	);
}
#else
#pragma message "Missing SSE2 instruction set, using slower fallback."
typedef uint8_t			t_ftable_128i[FT_CONTROL_SIZE];

static inline uint16_t	__get_occupied(
		t_ftable_128i ctrl
)
{
	int	i;
	int	out;

	out = 0;
	i = FT_CONTROL_SIZE;
	while (i--)
		if (control[i] & 0b10000000)
			out |= 1 << i;
	return ((uint16_t)out);
}
#endif

// Inserts a key-value inside a generic flat table.
// Hash needs to be computed outside of it.
//
// It's an unconditional insert in the table.
// Since it's possible for 2 items to have the same hash but you will have
// difficulties finding only the one over the other.
// It's not garantied that the first inserted will be the first to be found
// since the expantion shuffle the table.
bool			__ftable_insert(
		t_ftable_generic *table,
		void *key,
		void *value,
		size_t key_size,
		size_t value_size,
		t_ft_hash hash
)
{
	// Expand if we need to put a new pair
	if (table->pairs >= FT_LOAD_FACTOR * table->capacity)
		if (!__ftable_expand(table, table->capacity << 1, key_size, value_size))
			return (false);
	// Since the capacity is always in groups count and a power of 2,
	// we can simply replace the modulo with bit mask.
	size_t		index = hash.h1 & (table->capacity - 1);
	for (size_t i = 0; i < table->capacity; ++i) {
		// Start of the group we are searching in.
		// group {
		//     ctrl: 16 bytes (FT_GROUP_SIZE)
		//     keys: FT_GROUP_SIZE * key_size
		//     values: FT_GROUP_SIZE * value_size
		// }
		void *group = FT_GROUP_MEM(table->ctrl, index, key_size, value_size);

		// If there is a match in the group
		uint16_t match;
		if ((match = __get_occupied(*(t_ftable_128i*)group))) {
			for (size_t i = 0; i < FT_CONTROL_SIZE; ++i) {
				// Find which is the slot of the match
				if (match & (1 << i)) {
					// Inserts meta data into control
					((uint8_t*)group)[i] = 0;
					((uint8_t*)group)[i] = hash.h2;
					// Inserts the key-value
					memcpy(FT_KEY_MEM(group) + i * key_size, key, key_size);
					memcpy(FT_VALUE_MEM(group, key_size) + i * value_size, value, value_size);
					// Update table pairs
					table->pairs++;
					// Insert worked
					return (true);
				}
			}
		}
		index = (index + (1 << i)) & (table->capacity - 1);
	}
	// If we get there, there is no space left in the table.
	assert(!"We are NOT supposed to get there.");
}
