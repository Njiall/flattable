#include "flattable.h"

t_ft_hash		__ftable_hash(
		void *key,
		size_t capacity,
		size_t key_size
)
{
	// DJB2 implementation
	// TODO Replace with actual hash function
	t_ft_hash	hash = (t_ft_hash){.h = 5381};
	for (size_t i = 0; i < key_size; ++i) {
		hash.h = (hash.h << 5) + hash.h + ((uint8_t*)key)[i];
	}
	return (hash);
}
