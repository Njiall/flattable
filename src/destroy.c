#include "../include/flattable.h"
#include <stdlib.h>

bool		__ftable_destroy(
		t_ftable_generic *table
)
{
	if (table->data) {
		free(table->data);
		table->data = NULL;
		return (true);
	}
	return (false);
}
