include makefiles/debug.mk

#==============================================================================#
#                                 Flags detection                              #
#==============================================================================#

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
LDFLAGS+=$(foreach lib, $(SYSLIBS_LINUX), -l$(lib)) $(LDFLAGS_LINUX)
else ifeq ($(UNAME_S),Darwin)
LDFLAGS+=$(foreach lib, $(SYSLIBS_DARWIN), -l$(lib)) $(LDFLAGS_DARWIN)
endif
LDFLAGS+=$(foreach lib, $(SYSLIBS), -l$(lib))

#==============================================================================#
#                               Binary compilation                             #
#==============================================================================#

$(NAME): $(OBJS) $(CLIBS)
	@printf $(START_MSG)
	@printf $(MAKING_PROGRESS)
	@echo $(FLAGS)
	@echo $(SLOW_FLAG)
	@$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) $(LDLIBS) $(FLAGS) $(CLIBS) -fPIE; \
		if [ "$$?" != "1" ]; then \
			printf $(MAKING_SUCCESS); \
			exit 0; \
		else \
			printf $(MAKING_FAILURE); \
			exit 2; \
		fi
	@chmod +x $(NAME)
