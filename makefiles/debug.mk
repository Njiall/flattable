FAST_FLAG = -Ofast -march=native -flto
SLOW_FLAG = -fsanitize=address -g -O0

CFLAGS ?= -std=c18
FLAGS = $(CFLAGS)
ifeq	(,$(filter debug, $(MAKECMDGOALS)))
FLAGS += $(FAST_FLAG)
START_MSG = $(COMPILING_PRD)
else
FLAGS += $(SLOW_FLAG)
START_MSG = $(COMPILING_DBG)
endif
