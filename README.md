# <center>Flat Table</center>

> -- Njiall's broken editon

This is a generic implementation of the Google's Swisstable.
It's not perfect and it's syntax is not really pretty but it's a nice thing
to have when you want to design complex algorith in `C`.

## Disclaimer

This is a 'fun' project made to see where I could strech `C`'s types and macros
while doing something useful. The resulting code is not really readable.
But this language was not really meant to do it.
So if you want to blame someone, blame me.

This is a barebones project but it includes unit testing but you should
do your own.

Anyways, __DO NOT USE THIS IN PRODUCTION CODE__.
(Or make push requests so that it is safe to run on production projects :P)

## Supports

- Methods:
	- Insertion
	- Setting
	- Getting
	- Iteration
- Data types:
	- Static sized, any size
	- Variable size, any size

See by yourself.

## Example

```c
#include "include/flattable.h"
#include <stdio.h>

ftable_define(int, float);

int main() {
	int key;
	float value;

	ftable(int, float) table = ftable_new(int, float);
	if (!table.init(&table))
		return -1;

	// It supports inserts but you should not use this
	// As it can insert values with the same keys.
	table.insert(&table, &(int){0}, &(float){3.14159});

	// In it's place default to use `set` as it does the same thing
	// except it replaces the value with the new one
	// if the key is already stored.
	table.set(&table, &(int){1}, &(float){2.71828});

	// We can find that element with a key.
	if (table.find(&table, &(int){1}, &value))
		printf("Found: %f\n", value);

	// And we can iterate over the table.
	t_ftable_iterator it = ftable_iterator;
	while (table.iterate(&table, &it, &key, &value))
		printf("Found: %i -- %f\n", key, value);

	// And to free it's alocation we have to destroy it.
	table.destroy(&table);
}
```
