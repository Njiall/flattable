/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flattable.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/28 17:41:40 by njiall            #+#    #+#             */
/*   Updated: 2021/01/12 02:06:17 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLATTABLE_H
#define FLATTABLE_H

// License: DO WHAT THE FUCK YOU WANT WITH THIS I'M DONE.

// Hello there !
//
// This is a bit of a wierd project where I implement a swisstable in `c`.
// I'm trying to have the type checker work as well so
// it's gonna get complicated reaaaally fast because of macros.
//
// This is a `quadratic growth flat hash table`.
//
// 04-11-2020: Okay. Now, as with the world this is becoming really fucked up.
// SO. People looking at this trainwreck: Cpp exists, use it. DON'T do that.
// Oh and there is computational cost to this genericity.
// At least it's nice to the user's eyes (and fingers).
//
// 28-12-2020: Returning to this. This is fun. Now it should work. ... Possibly.
// I mean, excluding my retardness there's no way it wouldn't work right?
// The alignment is good right? Oh fuck. Now it's good. Prototypes OK?
// Shit. Welp that `void**` is stupid. I wanted to avoid memcpy but now that
// it is ~Generic~ I don't really have a choice. It's either that or the user
// will have to deal with it themselves. So it's that. TOO BAD :/
// Why the heck do I do this in `C`?
// And you would think that after that I would be done with `C`.
// But this tastes nice.
//
// 05-01-2021: 9 Bits enums HELL YEAH. I can be dumb sometimes.
// Now I'm sure the iteration and init proccess works.
// BUT WHY THE HELL WAS IT WORKING BEFORE???
// oh.
// Well it should work now.
//    Want to know know what i'm talking about? check the version log.
//    yeah you'll have to suffer.
// Now I'll tackle the resizing.
// But fuck I should redo the creation part.
//
// 12-01-2021: Now I'm working on generic keys.
// Meaning they are not of fixed length.
// This will include be made with:
//   - Constructor function
//   - Destructor function
//   - `Size finder`™ function
// I don't know where this will lead me but by the end I WILL have strings
// as keys.

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

// =============================================================================
// 					Explainations
// =============================================================================
//
// This is the explaination of how the data is stored.
//
// Data:
// The data is stored in a chain of buckets (groups) meta:
// - Meta-data
// - Keys
// - Values
//
// Meta-data:
// It's on the control part of the memory.
// There is 16 bytes on control composed of:
// - Hash: 7 bits
// 		This is used to do some quick comparison of the entries since the
// 		comparison function can be quite complex.
// - Status: 1 bit
// 		It indicates what is the status of the entry as could be:
// 			- Empty:   0b100000000
// 			- Used :   0b0xxxxxxxx
// 			- Deleted: 0b111111111
//
// Hashing:
// You can provide your own hashing function. And you should. You probably know
// better functions than I provide here (djb2).

// =============================================================================
// 					Utils
// =============================================================================

// Utils to concat the name of the types to the type of flat table.
// This needs to be called outside the scope of your fonctions.
#define __ftable_type(K, V) t_ftable_##K##_##V
#define __ftable_struct(K, V) struct s_ftable_##K##_##V

#define FT_DEFAULT_SIZE(K, V)	( \
		FT_GROUP_SIZE * sizeof(K) \
		+ FT_GROUP_SIZE * sizeof(V) \
		+ FT_CONTROL_SIZE \
)

#define FT_GROUP_MEM(_start, _idx, _ks, _vs) \
	(_start + (FT_CONTROL_SIZE + (_ks + _vs) * FT_GROUP_SIZE) * _idx)

// Size of the control part
#define FT_CONTROL_SIZE			(16)
// Size of a group
#define FT_GROUP_SIZE			FT_CONTROL_SIZE
// Factor determining the max percentage of filled table.
#define FT_LOAD_FACTOR			(.75)

typedef enum		e_ft_ctrl {
	// What an empty entry looks like.
	FT_EMPTY = 		0b10000000,
	// Deleted entry a.k.a. `Tombstone`.
	FT_DELETED = 	0b11111111,
	// Mask used to get the data part of the ctrl byte.
	FT_MASK = 		0b01111111,
}					t_ft_ctrl;

typedef union		u_ft_hash {
	struct {
		uint64_t	h1: 57;
		uint8_t		h2: 7;
	};
	uint64_t		h;
}					t_ft_hash;

// Iteration
typedef struct	s_ftable_iterator
{
	size_t index;
	size_t group;
}				t_ftable_iterator;

//typedef struct	s_ft_group_##K##_##V {
//	uint8_t		ctrl[FT_GROUP_SIZE];
//	K			keys[FT_GROUP_SIZE];
//	V			values[FT_GROUP_SIZE];
//}					t_ft_group;

// This is the generic part of the table where your types don't matter.
// For example the capacity or the allocated table.
#define __ftable_struct_generic_body \
	union { \
		void		**data; \
		uint8_t		*ctrl; \
	}; \
	/* Capacity is calculated as a multiple of groups */\
	size_t			capacity; \
	size_t			pairs; \
	t_ft_hash		(*hash)(void *, size_t); \
	bool			(*cmp)(void *, void *); \

typedef			struct s_ftable_generic {
	__ftable_struct_generic_body
}				t_ftable_generic;

// Macro to create the hashtable with correct type so there is type checking
// on access.
#define __ftable_proto(K, V) \
	__ftable_struct(K, V) { \
		/* Useful data types */ \
		/* This should be hidden and not used by the user */ \
		union { \
			void	**data; \
			uint8_t	*ctrl; \
		}; \
		/* Capacity is calculated as a multiple of groups */\
		size_t		capacity; \
		size_t		pairs; \
		\
		t_ft_hash	(*hash)(K*, size_t); \
		bool		(*cmp)(K*, K*); \
		/* External functions used by the user */ \
		/* Note: These are const to avoid user setting them */ \
		bool		(*const init)(__ftable_struct(K, V)*); \
		bool		(*const find)(__ftable_struct(K, V)*, K*, V*); \
		bool		(*const set)(__ftable_struct(K, V)*, K*, V*); \
		bool		(*const insert)(__ftable_struct(K, V)*, K*, V*); \
		bool		(*const remove)(__ftable_struct(K, V)*, K*, V*); \
		bool		(*const destroy)(__ftable_struct(K, V)*); \
		bool		(*const iterate)(__ftable_struct(K, V)*, t_ftable_iterator*, K*, V*); \
	} \

// =============================================================================
// 					Defining
// =============================================================================
//
// These macros are used to create a wrapper to define flattable types.
// They are simple to use as some parameters are optional.

// Wrapper to get a macro with n arguments
#define __MULTIPLEXER(_1, _2, _3, _4, NAME, ...) NAME

#define ftable_define(K, V) \
	/* Create the type for the table */ \
	__ftable_proto(K, V); \
	bool __ftable_init_##K##_##V(__ftable_struct(K, V)* table) { \
		return __ftable_init((t_ftable_generic *)table, sizeof(K), sizeof(V)); \
	} \
	bool __ftable_find_##K##_##V(__ftable_struct(K, V)* table, K* key, V* value) { \
		bool r = __ftable_find((t_ftable_generic *)table, \
					(void*)key, (void**)value, \
					sizeof(K), sizeof(V), \
					table->hash(key, table->capacity), \
					(table->cmp ? (bool (*)(void *, void *))table->cmp : __ftable_key_eq)); \
		return r; \
	} \
	bool __ftable_set_##K##_##V(__ftable_struct(K, V)* table, K* key, V* value) { \
		return (__ftable_set((t_ftable_generic *)table, \
					(void*)key, (void*)value, \
					sizeof(K), sizeof(V), \
					table->hash(key, table->capacity), \
					(table->cmp ? (bool (*)(void *, void *))table->cmp : __ftable_key_eq)) \
		); \
	} \
	bool __ftable_insert_##K##_##V(__ftable_struct(K, V)* table, K* key, V* value) { \
		return (__ftable_insert((t_ftable_generic *)table, \
					(void*)key, (void*)value, \
					sizeof(K), sizeof(V), \
					table->hash(key, table->capacity))\
		); \
	} \
	bool __ftable_remove_##K##_##V(__ftable_struct(K, V)* table, K* key, V* value) { \
		return (__ftable_remove((t_ftable_generic *)table, \
					(void*)key, (void*)value, \
					sizeof(K), sizeof(V), \
					table->hash(key, table->capacity), \
					(table->cmp ? (bool (*)(void *, void *))table->cmp : __ftable_key_eq))\
		); \
	} \
	bool __ftable_destroy_##K##_##V(__ftable_struct(K, V)* table) { \
		return (__ftable_destroy((t_ftable_generic *)table)); \
	} \
	bool __ftable_iterate_##K##_##V(__ftable_struct(K, V)* table, t_ftable_iterator *iter, K *key, V *value) { \
		return (__ftable_iterate((t_ftable_generic *)table, \
					iter, \
					(void**)key, (void**)value, \
					sizeof(K), sizeof(V) \
		)); \
	} \
	t_ft_hash __ftable_hash_##K##_##V(K* key, size_t capacity) { \
		return (__ftable_hash(key, capacity, sizeof(K))); \
	} \
	bool __ftable_cmp_##K##_##V(K* key, K* test) { \
		return (!memcmp(key, test, sizeof(K))); \
	} \
	typedef __ftable_struct(K, V) __ftable_type(K, V) \

#define ftable(K, V) __ftable_type(K, V)

#define __ftable_new_type(K, V) \
	(__ftable_struct(K, V)){ \
		.data = malloc(FT_DEFAULT_SIZE(K, V) * 16), .capacity = 16, \
		.init = (bool (*)(__ftable_struct(K, V)*))&__ftable_init_##K##_##V, \
		.find = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_find_##K##_##V, \
		.set = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_set_##K##_##V, \
		.insert = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_insert_##K##_##V, \
		.remove = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_remove_##K##_##V, \
		.destroy = (bool (*)(__ftable_struct(K, V)*))&__ftable_destroy_##K##_##V, \
		.iterate = (bool (*)(__ftable_struct(K, V)*, t_ftable_iterator*, K*, V*))&__ftable_iterate_##K##_##V, \
		.hash = (t_ft_hash (*)(K*, size_t))&__ftable_hash_##K##_##V, \
		.cmp = (bool (*)(K*, K*))&__ftable_cmp_##K##_##V, \
	}
#define __ftable_new_type_cmp(K, V, cmp) \
	(__ftable_struct(K, V)){ \
		.data = malloc(FT_DEFAULT_SIZE(K, V) * 16), .capacity = 16, \
		.cmp = cmp \
		.init = (bool (*)(__ftable_struct(K, V)*))&__ftable_init_##K##_##V, \
		.find = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_find_##K##_##V, \
		.insert = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_insert_##K##_##V, \
		.remove = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_remove_##K##_##V, \
		.destroy = (bool (*)(__ftable_struct(K, V)*))&__ftable_destroy_##K##_##V, \
		.iterate = (bool (*)(__ftable_struct(K, V)*, t_ftable_iterator*, K*, V*))&__ftable_iterate_##K##_##V, \
		.hash = (t_ft_hash (*)(K*, size_t))&__ftable_hash_##K##_##V, \
	}
#define __ftable_new_type_cmp_hash(K, V, cmp, hash) \
	(__ftable_struct(K, V)){ \
		.data = malloc(FT_DEFAULT_SIZE(K, V) * 16), .capacity = 16, \
		.cmp = cmp, .hash = hash \
		.init = (bool (*)(__ftable_struct(K, V)*))&__ftable_init_##K##_##V, \
		.find = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_find_##K##_##V, \
		.insert = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_insert_##K##_##V, \
		.remove = (bool (*)(__ftable_struct(K, V)*, K*, V*))&__ftable_remove_##K##_##V, \
		.destroy = (bool (*)(__ftable_struct(K, V)*))&__ftable_destroy_##K##_##V, \
		.iterate = (bool (*)(__ftable_struct(K, V)*, t_ftable_iterator*, K*, V*))&__ftable_iterate_##K##_##V, \
		.hash = (t_ft_hash (*)(K*))&hash, \
	}

// Create a template struct to init
// /!\ This will allocate memory you should check if the result is valid!
#define ftable_new(...) __MULTIPLEXER(__VA_ARGS__, __ftable_new_type_cmp_hash, __ftable_new_type_cmp, __ftable_new_type)(__VA_ARGS__)

#define FT_VALUE_MEM(group, key_size) (((uint8_t*)group) + FT_CONTROL_SIZE + FT_GROUP_SIZE * key_size)
#define FT_KEY_MEM(group) (((uint8_t*)group) + FT_CONTROL_SIZE)

// =============================================================================
// 					Internal API
// =============================================================================
// These functions are made to be size agnostic so that you don't have to
// use any sizes with your function calls.
// The macro ftable_new will make types and functions for you.

bool		__ftable_init(
		t_ftable_generic *table,
		size_t key_size,
		size_t value_size
);

// Generic find on the table with sizes as inputs
bool		__ftable_find(
		t_ftable_generic *table,
		void *key,
		void **value,
		size_t key_size,
		size_t value_size,
		t_ft_hash hash,
		bool (*)(void *, void *)
);

// Generic set on the table with sizes as inputs
bool		__ftable_set(
		t_ftable_generic *table,
		void *key,
		void *value,
		size_t key_size,
		size_t value_size,
		t_ft_hash hash,
		bool (*cmp)(void *, void *)
);

// Generic unconditional insert on the table with sizes as inputs
bool		__ftable_insert(
		t_ftable_generic *table,
		void *key,
		void *value,
		size_t key_size,
		size_t value_size,
		t_ft_hash hash
);

bool		__ftable_remove(
		t_ftable_generic *table,
		void *key,
		void **value,
		size_t key_size,
		size_t value_size,
		t_ft_hash hash,
		bool (*cmp)(void *, void *)
);

bool		__ftable_destroy(
		t_ftable_generic *table
);

t_ft_hash	__ftable_hash(
		void *,
		size_t capacity,
		size_t size
);

// Returns the default iterator
// if you want to use this in an iteration:
// void *iter = ftable_iterator;
// and then pass it's address to the iteration function.
#define ftable_iterator (t_ftable_iterator){.group = 0, .index = 0}

// void *it = ftable_iterator;
// V *value;
// while ((value = ftable_iterate(&table, &it, sizeof(K), sizeof(V))))
// { ... }
bool		__ftable_iterate(
		t_ftable_generic *table,
		t_ftable_iterator *iterator,
		void **key,
		void **value,
		size_t key_size,
		size_t value_size
);

bool		__ftable_expand(
		t_ftable_generic *table,
		size_t new_capacity,
		size_t key_size,
		size_t value_size
);

bool		__ftable_key_eq(
		void *,
		void *
);
#endif
